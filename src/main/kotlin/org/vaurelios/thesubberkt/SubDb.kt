package org.vaurelios.thesubberkt

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.*
import io.reactivex.Observable
import tornadofx.error
import tornadofx.information
import tornadofx.runLater
import java.io.File

object SubDb {
  init {
    FuelManager.instance.basePath = "http://api.thesubdb.com"
    FuelManager.instance.baseHeaders = mapOf("User-Agent" to "SubDB/1.0 (TheSubberKt/0.1; https://gitlab.com/vaurelios/TheSubberKt)")
  }

  fun searchByHash(hash: String): Observable<String> {
    return Observable.create<String> { s ->
      Fuel.get("/", listOf("action" to "search", "hash" to hash, "versions" to "")).responseString { request, response, result ->
        result.fold({ value ->
          for (lang in value.split(",")) s.onNext(lang)
          s.onComplete()
        }, { error ->
          // the api response with 404 when there's no subtitle for hash
          if (response.statusCode == 404)
            s.onComplete()
          else
            s.onError(Exception("SubDb Error: ${error.message} | URL: ${request.url}"))
        })
      }
    }
  }

  fun download(hash: String, sub: Subtitle, destPath: String) {
    Fuel.download("/", Method.GET, listOf("action" to "download", "hash" to hash, "language" to sub.lang))
        .fileDestination { _, _ ->
          File(destPath)
        }.progress { readBytes, totalBytes ->
          println("Download: ${(readBytes / totalBytes) * 100}%")
        }.response { _, _, _ -> }
  }

  fun upload(hash: String, file: File) {
    Fuel.upload("/", Method.POST, listOf("action" to "upload", "hash" to hash))
        .add(InlineDataPart(hash, name = "hash"))
        .add(FileDataPart(file, name = "file"))
        .progress { readBytes, totalBytes ->
          println("Download: ${(readBytes / totalBytes) * 100}%")
        }
        .response { result ->
          result.fold(
              success = {
                runLater {
                  information("Success", "Subtitle file uploaded successful!")
                }
              },
              failure = { fuelError ->
                val response = fuelError.response

                println("Status Code: ${response.statusCode}\nCause: ${response.responseMessage}")

                when (response.statusCode) {
                  400 -> runLater {
                    error("Duplicate", "The file you're trying to upload is a duplicate!")
                  }
                  403 -> runLater {
                    error("Duplicate", "The file you're trying to upload is a duplicate!")
                  }
                  415 -> runLater {
                    error("Invalid Format", "The file type you're trying to upload is unsupported!")
                  }
                  else -> runLater {
                    error("Unknown Error", "A unknown error was occurred!")
                  }
                }
              })
        }
  }
}