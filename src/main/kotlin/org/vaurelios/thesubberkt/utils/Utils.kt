package org.vaurelios.thesubberkt.utils

import com.google.common.hash.Hashing
import java.io.File
import java.io.RandomAccessFile

@Suppress("DEPRECATION")
fun hashForMovieFile(mediaFile: File): String {
  val hf = Hashing.md5()
  val hc = hf.newHasher()
  val file = RandomAccessFile(mediaFile, "r")
  val readsize = 64 * 1024

  if (file.length() >= 2 * readsize) {
    for (i in 1..readsize) hc.putByte(file.readByte())
    file.seek(file.length() - readsize)
    for (i in 1..readsize) hc.putByte(file.readByte())

    return hc.hash().toString()
  }

  return "<INVALID_FILE_LENGTH>"
}
