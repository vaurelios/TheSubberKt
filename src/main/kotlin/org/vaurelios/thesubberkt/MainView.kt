package org.vaurelios.thesubberkt

import io.reactivex.rxkotlin.subscribeBy
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.scene.control.Alert
import javafx.scene.layout.Priority
import javafx.stage.FileChooser
import org.vaurelios.thesubberkt.utils.hashForMovieFile
import tornadofx.*
import java.io.File

class MainView: View("TheSubberKt") {
  override val root = Form()
  val subs = FXCollections.observableArrayList<Subtitle>()
  val model: MainViewModel by inject()

  init {
    with(root) {
      prefWidth = 400.0

      fieldset {
        field("Media:") {
          textfield(model.mediaPath) {
            editableProperty().value = false
          }.required()
          button("...") {
            action {
              setFile(chooseFile("Select a media file...", arrayOf(FileChooser.ExtensionFilter("Media Files (*.*)", "*.*")))[0])
            }
          }
        }
      }
      tableview(subs) {
        column("Versions", Subtitle::versions)
        column("Language", Subtitle::lang)
        column("Media Name", Subtitle::mediaName)

        columnResizePolicy = SmartResize.POLICY

        bindSelected(model.selectedSubtitle)
      }
      hbox {
        spacing = 5.0

        button("Upload") {
          hgrow = Priority.ALWAYS
          maxWidth = Double.MAX_VALUE

          enableWhen(model.mediaPath.isNotEmpty)

          action {
            showUploadDialog()
          }
        }
        button("Hash Search") {
          hgrow = Priority.ALWAYS
          maxWidth = Double.MAX_VALUE

          enableWhen(model.mediaPath.isNotEmpty)

          action {
            searchByHash(model.mediaHash.value)
          }
        }
        button("Download Selected") {
          hgrow = Priority.ALWAYS
          maxWidth = Double.MAX_VALUE

          enableWhen(model.selectedSubtitle.isNotNull)

          action {
            val srtFilePath = model.mediaFile.value.absolutePath.replace(Regex(".[^.]+$"), ".srt")
            downloadSelected(model.mediaHash.value, model.selectedSubtitle.value, srtFilePath)
          }
        }
      }
    }
  }

  fun setFile(file: File) {
    if (file.exists()) {
      model.mediaFile.value = file
      model.mediaPath.value = file.absolutePath
      model.mediaHash.value = hashForMovieFile(model.mediaFile.value)
    }
    else {
      Platform.runLater {
        error("File not found", "File '${file.absolutePath}' was not found...")
      }
    }
  }

  private fun searchByHash(hash: String) {
    subs.clear()

    SubDb.searchByHash(hash).subscribeBy(
        onNext = {
          val (lang, versions) = it.split(":")

          subs.add(Subtitle(lang, versions.toIntOrNull() ?: 1, model.mediaFile.value.name))
        },
        onComplete = {
          if (subs.size == 0)
            Platform.runLater {
              alert(Alert.AlertType.WARNING, "No subtitles found", "No subtitles was found for the selected media file")
            }
        },
        onError = {
          Platform.runLater {
            error("An error has occurred", "An error has occurred while searching for subtitles.\nPlease check console output.")
          }
        }
    )
  }

  private fun downloadSelected(hash: String, sub: Subtitle, destPath: String) {
    println("Download to: $destPath")
    SubDb.download(hash, sub, destPath)
  }

  private fun showUploadDialog() {
    openInternalWindow<UploadView>()
  }
}

class MainViewModel: ViewModel() {
  val mediaPath = SimpleStringProperty(this, "mediaPath")
  val mediaFile = bind { SimpleObjectProperty<File>(this, "mediaFile") }
  val mediaHash = bind { SimpleStringProperty(this, "mediaHash") }
  val selectedSubtitle = SimpleObjectProperty<Subtitle>(this, "selectedSubtitle")
}
