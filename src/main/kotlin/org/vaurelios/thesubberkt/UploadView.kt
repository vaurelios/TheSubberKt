package org.vaurelios.thesubberkt

import javafx.application.Platform
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.layout.Priority
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File

class UploadView: View("Upload Subtitle") {
  private val model = UploadViewModel()
  private val mainModel: MainViewModel by inject()

  override val root = Form()

  init {
    with(root) {
      fieldset {
        field("Subtitle File") {
          textfield(model.subtitlePath) {
            editableProperty().value = false
          }
          button("...") {
            action {
              setFile(chooseFile("Select a media file...", arrayOf(FileChooser.ExtensionFilter("Media Files (*.*)", "*.*")))[0])
            }
          }
        }
        field("Media Hash:") {
          label(mainModel.mediaHash)
        }
        field("Subtitle Size:") {
          label(model.subtitleSize)
        }
      }
      hbox {
        button("Cancel") {
          hgrow = Priority.ALWAYS
          maxWidth = Double.MAX_VALUE

          action {
            close()
          }
        }
        button("Upload") {
          hgrow = Priority.ALWAYS
          maxWidth = Double.MAX_VALUE

          action {
            upload()
            close()
          }
        }
      }
    }
  }

  private fun setFile(file: File) {
    if (file.exists()) {
      model.subtitleFile.value = file
      model.subtitlePath.value = file.absolutePath
      model.subtitleSize.value = file.length()
    }
    else {
      Platform.runLater {
        error("File not found", "File '${file.absolutePath}' was not found...")
      }
    }
  }

  private fun upload() {
    SubDb.upload(mainModel.mediaHash.value, model.subtitleFile.value)
  }
}

class UploadViewModel: ViewModel() {
  val subtitleFile = SimpleObjectProperty<File>(this, "subtitleFile")
  val subtitlePath = SimpleStringProperty(this, "subtitleFile")
  val subtitleSize = SimpleLongProperty(this, "subtitleSize")
}