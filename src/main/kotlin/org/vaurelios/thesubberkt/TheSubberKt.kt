package org.vaurelios.thesubberkt

import tornadofx.App
import tornadofx.find
import tornadofx.launch
import java.io.File

class TheSubberKt: App(MainView::class) {
  override fun init() {
    super.init()

    val view = find(MainView::class, scope)

    if (parameters.unnamed.size > 0)
      view.setFile(File(parameters.unnamed[0]))
  }
}

fun main(args: Array<String>) = launch<TheSubberKt>(args)