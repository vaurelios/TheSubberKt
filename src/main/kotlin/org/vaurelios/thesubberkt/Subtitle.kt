package org.vaurelios.thesubberkt

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.getValue
import tornadofx.setValue

class Subtitle(lang: String? = null, versions: Int = 0, mediaName: String? = null) {
  val langProperty = SimpleStringProperty(this, "lang", lang)
  var lang by langProperty
  val versionsProperty = SimpleIntegerProperty(this, "versions", versions)
  var versions by versionsProperty
  val mediaNameProperty = SimpleStringProperty(this, "mediaName", mediaName)
  var mediaName by mediaNameProperty
}
